# Readme.md

OpenTap.Plugins.Interfaces project contains all visible interfaces it is referenced in all instrument driver implementations.
Plugins.Oscar.Shared is a shared project that contains Trace namespace used to get info of a calling method and FTP namespace that implements 
ftp which is used to copy files from PC to instrument. 

# TAP drivers for following instrument models:  

## AC Power supplies 
- Ametek AST1501
- Keysight AC6800
- Kikusui PCR500M/PCR1000M/PCR2000M/PCR4000M

## Frequence reference's
- Keysight Pxi M9300a

## Electronic load's
- Chroma electronic load - Specify model!

## LED analyzers's
- Cat LED analyzer - Specify model!
- NI dio card reading Hamamatsu S9706 color sensors
- Feasa - Specify model!

## Passive intermodulation
- Rosenberger - Specify model

## Power meter's
- Keysight U2022XA
- Rohde&Schwarz NRP8S, NRP40S, NRP50S

## DC Power supplies
- TDK Lambda
- Keysight N57xx series
- Keysight N67xx series

## Pulse Generator's
- Keysight 81150A

## Signal generator's
- Keysight EXG series
- Keysight MXG series
- Rohde&Schwarz SMA
- Rohde&Schwarz Smate
- Rohde&Schwarz SGT100A
- Rohde&Schwarz SMW

# Nuget plugins needed for compilation
- Newtonsoft Json 12.0.1
- SSH.NET 2016.1.0
- System.ValueTuple 4.3.0