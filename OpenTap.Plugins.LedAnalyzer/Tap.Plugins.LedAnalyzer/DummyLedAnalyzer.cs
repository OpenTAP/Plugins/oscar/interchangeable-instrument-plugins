﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using OpenTap.Plugins.Interfaces.LedAnalyzer;

namespace OpenTap.Plugins.LedAnalyzer
{
    [Display("DummyLedAnalyzer", Group: "LED Analyzer", Description: "Dummy LED Analyzer driver")]
    public class DummyLedAnalyzer : Instrument, ILedAnalyzer
    {
        public DummyLedAnalyzer()
        {
            Name = "DummyLedAnalyzer";
        }

        public void Initialize()
        {
            Log.Info("DummyLedAnalyzer - Initialize");
        }

        public void CaptureData(EBrightnessLevel brightnessLevel)
        {
            Log.Info("DummyLedAnalyzer - CaptureData");
        }

        public void CaptureDataPwm(EBrightnessLevel brightnessLevel, int averageFactor = 0)
        {
            Log.Info("DummyLedAnalyzer - CaptureDataPwm");
        }

        public RgbData GetRgb(int channel)
        {
            Log.Info("DummyLedAnalyzer - GetRgb");
            return new RgbData { Blue = byte.MaxValue, Green = byte.MaxValue, Red = byte.MaxValue };
        }

        public List<RgbData> GetRgbs()
        {
            throw new NotImplementedException();
        }

        public HsiData GetHsi(int channel)
        {
            Log.Info("DummyLedAnalyzer - GetHsi");
            return new HsiData { Hue = 10, Intensity = 20, Saturation = 30};
        }

        public List<HsiData> GetHsiGroup(int channelgroup)
        {
            Log.Info("DummyLedAnalyzer - GetHsiGroup");
            return new List<HsiData>
            {
                new HsiData {Hue = 10, Intensity = 20, Saturation = 30},
                new HsiData {Hue = 40, Intensity = 50, Saturation = 60},
                new HsiData {Hue = 70, Intensity = 80, Saturation = 90}
            };
        }

        public ChromaticityData GetChromaticity(int channel)
        {
            Log.Info("DummyLedAnalyzer - GetChromaticity");
            return new ChromaticityData { XChromaticity = 123, YChromaticity = 345};
        }

        public void Initialize(JObject configElement)
        {
            throw new NotImplementedException();
        }
    }
}
