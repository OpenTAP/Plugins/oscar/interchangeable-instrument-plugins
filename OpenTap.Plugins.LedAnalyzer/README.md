# Nokia.Tap.Driver.LedAnalyzer
This project implements LedAnalyzer driver for Tap
Support Feasa Led Analyzer and iCat Led Analyzer

## Getting Started
Tap developer guide: https://www.keysight.com/upload/cmc_upload/All/TapDeveloperGuide.pdf
This is useful, if edit this file: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

### Prerequisites
Installed SW: 
Keysight Tap

### Installing
Download this project from GITlab to edit it. 

## List of methods available in interface:
See ILedAnalyzer.cs

## Built With
Visual studio 2017

## Contributing

## Versioning
We use [SemVer](http://semver.org/) for versioning. For the versions available, 
see the [tags on this repository](https://gitlabe1.ext.net.nokia.com/oscar/Tap.Drivers/Nokia.Tap.Driver.LedAnalyzer/tags) 

## Authors
* **Oscar Team** - *Initial work* - (https://gitlabe1.ext.net.nokia.com/groups/oscar/group_members)
* See also the list of [contributors](https://gitlabe1.ext.net.nokia.com/oscar/Tap.Drivers/Nokia.Tap.Driver.LedAnalyzer/graphs/master) who participated in this project.

## License
