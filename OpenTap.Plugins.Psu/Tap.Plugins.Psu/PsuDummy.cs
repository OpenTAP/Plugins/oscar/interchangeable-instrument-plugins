﻿using System;
using OpenTap.Plugins.Interfaces.PSU;

namespace OpenTap.Plugins.Psu
{
    // No need to have any actual implementation since base class works as it is.
    public class PsuDummy : PsuBase
    {
        public override void Close()
        {
            Log.Info("PsuDummy : Close");
        }

        public override void Open()
        {
            Log.Info("PsuDummy : Open");
        }

        protected override void ScpiCmd_SetCurr(double currentAmps, EModule module = EModule.Module1)
        {
            lock (_instrumentLock)
            {
                Log.Info("PsuDummy:ScpiCmd_SetCurr");
            }
        }

        public override double GetCurrentLimit(EModule module = EModule.Module1)
        {
            throw new NotImplementedException();
        }

        protected override void ScpiCmd_SetVolt(double voltageVolts, EModule module = EModule.Module1)
        {
            throw new NotImplementedException();
        }

        public override double GetVoltageLevel(EModule module = EModule.Module1)
        {
            throw new NotImplementedException();
        }

        public override double GetVoltageLevelMax(EModule module = EModule.Module1)
        {
            throw new NotImplementedException();
        }

        public override void SetOverVoltageLevel(double voltage, EModule module = EModule.Module1)
        {
            throw new NotImplementedException();
        }

        public override double GetOverVoltageLevel(EModule module = EModule.Module1)
        {
            throw new NotImplementedException();
        }

        public override double GetOverVoltageLevelMax(EModule module = EModule.Module1)
        {
            throw new NotImplementedException();
        }

        public override void SetPolarity(EPolarity polarity, EModule module = EModule.Module1)
        {
            throw new NotImplementedException();
        }

        public override void SetMeterView(EView view)
        {
            throw new NotImplementedException();
        }

    }
}
