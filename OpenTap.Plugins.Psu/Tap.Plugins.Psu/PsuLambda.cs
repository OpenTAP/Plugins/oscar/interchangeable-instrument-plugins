using System;
using System.Globalization;
using OpenTap.Plugins.Interfaces.PSU;

namespace OpenTap.Plugins.Psu
{
    [Display("TDK Lambda PSU driver", Group: "OpenTap.Plugins", Description: "TDK Lambda PSU driver")]
    public class PsuLambda : PsuBase
    {
        public PsuLambda()
        {
            Name = "TDK Lambda";
        }
        /// <inheritdoc />
        public override void Open()
        {
            lock (_instrumentLock)
            {
                MaxVoltageV = double.NaN;
                MaxCurrentA = double.NaN;

                if (IsConnected) throw new InvalidOperationException("Psu is already connected!");

                base.Open();

                var response = ScpiQuery("*TST?");
                if (response != "0")
                    throw new ApplicationException("Lambda PSU returned " + response +
                                                   " from self test. PSU faulty!");

                response = ScpiQuery("*IDN?"); // TypicResp: LAMBDA,GEN30-80-LAN,S/N:705A253-0001,1U2K:5.1.1-LAN:2.1
                string[] separators = {",GEN", "-"};
                string[] words = response.Split(separators, StringSplitOptions.RemoveEmptyEntries);

                MaxVoltageV = Convert.ToDouble(words[1]);
                Log.Info("Equipment MAX voltage is: " + MaxVoltageV);

                MaxCurrentA = Convert.ToDouble(words[2]);
                Log.Info("Equipment MAX current is: " + MaxCurrentA);
            }
        }

        /// <inheritdoc />
        public override void Close()
        {
            lock (_instrumentLock)
            {
                Log.Info("PsuLambda: Close");
                base.Close();
            }
        }

        /// <inheritdoc />
        protected override void ScpiCmd_SetVolt(double voltageVolts, EModule module = EModule.Module1)
        {
            lock (_instrumentLock)
            {
                ScpiCommand(":VOLT " + voltageVolts.ToString("#.000", CultureInfo.InvariantCulture) + "");
            }
        }

        /// <inheritdoc />
        public override double GetVoltageLevel(EModule module = EModule.Module1)
        {
            lock (_instrumentLock)
            {
                return ScpiQuery<double>(":VOLT?");
            }
        }

        /// <inheritdoc />
        public override double GetVoltageLevelMax(EModule module = EModule.Module1)
        {
            throw new Exception("Command is not supported YET! Add this to driver if need to use.");
        }

        public override void SetOverVoltageLevel(double voltage, EModule module = EModule.Module1)
        {
            throw new NotImplementedException();
        }

        public override double GetOverVoltageLevel(EModule module = EModule.Module1)
        {
            throw new NotImplementedException();
        }

        public override double GetOverVoltageLevelMax(EModule module = EModule.Module1)
        {
            throw new NotImplementedException();
        }

        public override void SetPolarity(EPolarity polarity, EModule module = EModule.Module1)
        {
            throw new NotImplementedException();
        }

        public override void SetMeterView(EView view)
        {
            throw new Exception("Command is not supported!");
        }

        /// <inheritdoc />
        protected override void ScpiCmd_SetCurr(double currentAmps, EModule module = EModule.Module1)
        {
            lock (_instrumentLock)
            {
                ScpiCommand(":CURR " + currentAmps.ToString("#.000", CultureInfo.InvariantCulture));
            }
        }

        /// <inheritdoc />
        public override double GetCurrentLimit(EModule module = EModule.Module1)
        {
            lock (_instrumentLock)
            {
                return ScpiQuery<double>(":CURR?");
            }
        }
    }
}