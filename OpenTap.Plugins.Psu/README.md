# Nokia.Tap.Driver.Psu
This project implements DC PSU driver for Tap
This driver supports different equipments and those uses same methods. All equipments do not support all methods. Only used methods have been implemented.

## Getting Started
Tap developer guide: https://www.keysight.com/upload/cmc_upload/All/TapDeveloperGuide.pdf
This is useful, if edit this file: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
Readme available https://gitlabe1.ext.net.nokia.com/oscar/Nokia.Tap.Driver.Psu/README.md

### Prerequisites
Installed SW: VISA 

### Installing
Download this project from GITlab to edit it. 

## List of methods available in interface:
See IPsu.cs

## Deployment
Typically stdTests uses this Driver as nuget.

## Built With
Visual studio 2017

## Contributing

## Versioning
We use [SemVer](http://semver.org/) for versioning. For the versions available, 
see the [tags on this repository](https://gitlabe1.ext.net.nokia.com/oscar/Nokia.Tap.Driver.I2CSpi/tags). 

## Authors
* **Oscar Team** - *Initial work* - (https://gitlabe1.ext.net.nokia.com/groups/oscar/group_members)
* See also the list of [contributors](https://gitlabe1.ext.net.nokia.com/oscar/Nokia.Tap.Driver.I2CSpi/graphs/master) who participated in this project.

## License

## Acknowledgments

## NOTEs for refactoring Master/Slave (multipower using 2x N5767)
Now stdTest have own tests for single power and Master/slave power: MasterSlavePowerUpTest & MasterSlavePowerControl
Master/Slave tests could be refactored/implemented to sigle power tests with next changes to this driver:
- Add Slave power to opening/interface as enabled function and use it to separate single/multipower-case.
- Most of commands must be duplicated to both Master and Slave: reset, selftest ID-query
- SetCurrent value must be divided by number of equipments, but command is sent to all units separately.
- Over Current Protection set: Master ON / Slave OFF.
- Slave Over Voltage Protection set must be higher than Master. Started to use with 3VDC. Wait for response, if malfunctions occurs with 3VDC.
- Slave set Voltage value must be higher than Master. Started to use with 5VDC more than master. 
  5V difference need to reduce with high voltage values to get over 60VDC output. Waiting for response, if malfunctions occurs with 5 and 1.5VDC.
- Voltage ON is set first to Slave , then Master. Slave voltage will follow the master.
  Master/Slave setup could be checked with voltage ON. If HW not connected correctly, will slave voltage raise before Master is set ON.


