﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using OpenTap.Plugins.Interfaces.Common;
using OpenTap.Plugins.Interfaces.SG;

namespace OpenTap.Plugins.Sg
{
    [Display("Rohde&Schwarz SGT100A driver", Group: "OpenTap.Plugins", Description: "Rohde & Schwarz Signal generator")]
    public class RohdeSchwarzSgt100a : SgRohdeSchwarzSmw
    {
        #region Settings
        #endregion
        public RohdeSchwarzSgt100a()
        {
            Name = "SGT100A";
        }

        public override void SetTriggerPolarity(ETriggerPolarity polarity)
        {
            // TODO, need updated
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public override void SetSelectArbFile(string fileFolderAndName, double awgSampleClockMhz)
        {
            if (Name != string.Empty)
            {
                // load does not work with long filename like: "/var/user/5G_SIGNALS/64QAM_cellid1_papr13"
                ScpiCommand("BB:ARB:WAV:SEL \"" + fileFolderAndName + "\";*WAI");
                ScpiCommand("BB:ARB:CLOC "+ awgSampleClockMhz * 1E6);
                ScpiCommand("SOUR:BB:ARB:STAT ON;*WAI");
            }
            else
            {
                throw new ArgumentException("folder or patternName is empty!");
            }
        }

        public override void SetModulationState(EState modulationState)
        {
            ScpiCommand("SOUR:IQ:STAT " + Scpi.Format("{0}", modulationState) + ";*WAI");
        }

        protected override void CheckIfArbFileExists(EState arbFileCheckState)
        {
            const string ftpFolder = "/share/";
            const string scpiFolder = "\\var\\user\\";
            if (arbFileCheckState != EState.On) return;
            foreach (var arb in ArbFileList)
                if (arb == "ArbOne")
                {
                    Log.Info("SGT100A: Empty arbfile list");
                }

                else
                {
                    if (!ArbFileFolder.EndsWith("/"))
                        ArbFileFolder += "/";

                    ScpiCommand("MMEM:CDIR \"" + scpiFolder + ArbFileFolder + "\";*WAI");
                    //TODO: Can made little bit faster if qurey is outside loop
                    var instrumentFiles = ScpiQuery(":MMEM:CAT?");
                    if (instrumentFiles.Contains(arb)) continue;
                    Log.Info("SGT100A:Need to transfer file from PC.File:  " + arb);
                    var tmpAddress = VisaAddress.Split(':');
                    var address = tmpAddress[2]; //"TCPI0::"
                    //192.168.255.xxx =maxLenght
                    const int maxLenght = 15;
                    address = string.Concat(address.Take(maxLenght));
                    var ftpArbFolderAndFileName = ftpFolder + ArbFileFolder + arb;

                    // Skip the folder exits chcek, it's always error in SGT100A
                    // Force to upload file

                    _ftp.SendFile(address,
                        FtpUserName,
                        FtpPassword, false,
                        ftpArbFolderAndFileName,
                        LocalArbFileFolder + "\\" +
                        arb);
                }
        }
        /// <summary>
        /// Open procedure for the instrument.
        /// </summary>
        public override void Open()
        {
            base.Open();
        }

        /// <summary>
        /// Close procedure for the instrument.
        /// </summary>
        public override void Close()
        {
            // TODO:  Shut down the connection to the instrument here.
            base.Close();
        }
    }
}
