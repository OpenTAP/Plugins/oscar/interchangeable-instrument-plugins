﻿using System;
using System.Globalization;

namespace OpenTap.Plugins.Sg
{
    [Display("Keysight EXG driver", Group: "OpenTap.Plugins", Description: "Keysight EXG driver")]
    public class SgKeysightExg : SgKeysightMxg
    {
        public SgKeysightExg()
        {
            ModelName = "EXG";
            Name = "EXG";
        }

        /// <inheritdoc />
        public override void SetFrequency(double freqInMHz)
        {
            if (freqInMHz <= 0) throw new ArgumentOutOfRangeException(nameof(freqInMHz));
            ScpiCommand("FREQ " + freqInMHz.ToString("#.000000", CultureInfo.InvariantCulture) + "MHZ");
        }

        /// <inheritdoc />
        public override decimal GetFrequency()
        {
            return ScpiQuery<decimal>("FREQ?");
        }

        /// <inheritdoc />
        public override void SetRuntimeScaling(double scale)
        {
            if (scale < 1 || scale > 100) throw new ArgumentOutOfRangeException(nameof(scale));
            ScpiCommand("RAD:ARB:RSC " + scale.ToString("#.00", CultureInfo.InvariantCulture) );
        }
    }
}