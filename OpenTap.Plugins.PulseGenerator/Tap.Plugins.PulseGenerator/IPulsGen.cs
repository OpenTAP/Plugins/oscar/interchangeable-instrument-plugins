﻿using System.Collections.Generic;
using Keysight.Tap;

namespace Nokia.Tap.Driver.PulseGenerator
{
    public enum EState
    {
        [Scpi("OFF")] Off,
        [Scpi("ON")] On
    }

    public enum EErrorOccurred
    {
        No,
        Yes
    }

    public enum EopcStatus
    {
        Incomplete = 0,
        Complete = 1
    }

    /// <summary>
    ///     Pulse Func Arb generator arming sources:
    ///     ArmsourceInt = source internal.
    ///     ArmsourceExt = source external.
    ///     ArmsourceMan = source manual.
    ///     ArmsourceImm = continuous mode.
    /// </summary>
    public enum EPulsGenArmingSource
    {
        [Scpi("INT")] Internal,
        [Scpi("EXT")] External,
        [Scpi("MAN")] Manual,
        [Scpi("IMM")] Immediate
    }

    /// <summary>
    ///     Pulse Func Arb generator output types:
    ///     FuncOutSinu = sine wave.
    ///     FuncOutSqu = square wave.
    ///     FuncOutRamp = ramp wave.
    ///     FuncOutNois = noise.
    ///     FuncOutDc = DC.
    /// </summary>
    public enum EPulsGenFuncOutput
    {
        [Scpi("SINU")] Sinusoid,
        [Scpi("SQU")]  Square,
        [Scpi("RAMP")] Ramp,
        [Scpi("PULS")] Pulse,
        [Scpi("NOIS")] Noise,
        [Scpi("DC")] Dc
    }

    /// <summary>
    ///     Pulse Func Arb generator modes:
    ///     TrigMode = triggered mode.
    ///     GatMode  = gated mode.
    ///     ImmMode  = continuous mode.
    /// </summary>
    public enum EPulsGenMode
    {
        [Scpi("TRIG")] TriggerMode,
        [Scpi("GAT")] GateMode,
        [Scpi("IMM")] ContMode
    }

    /// <summary>
    ///     Pulse Func Arb generator output polarity:
    ///     PolarityNorm = normal.
    ///     PolarityInv  = inverted.
    /// </summary>
    public enum EPulsGenPolarity
    {
        [Scpi("NORM")] Normal,
        [Scpi("INV")] Inverted
    }

    public enum EPulsGenOutImp
    {
        [Scpi("50")] Ohm50,
        [Scpi("5")] Ohm5
    }


    public interface IPulsGen : IInstrument
    {
        /// <summary>
        ///     Returns all the errors on the instrument error stack. Clears the list in
        ///     the same call.
        /// </summary>
        /// <param name="suppressLogMessages">if true the errors will not be logged</param>
        /// <param name="maxErrors">
        ///     The max number of errors to retrieve. Useful if instrument generates errors faster than they
        ///     can be read.
        /// </param>
        /// <returns></returns>
        List<ScpiInstrument.ScpiError> QueryErrors(bool suppressLogMessages = false, int maxErrors = 1000);

        /// <summary>
        ///     Set arming source to instrument.
        /// </summary>
        void SetArmingSource(EPulsGenArmingSource armingSource);

        /// <summary>
        ///     Set output type to instrument.
        /// </summary>
        void SetFuncOutput(EPulsGenFuncOutput outputType);

        /// <summary>
        ///     Sets continous and trigger state to the instrument.
        ///     Not implemented yet, because not needed in drone tests.
        /// </summary>
        /// <param name="triggeredMode">Trigger mode on or off.</param>
        /// <param name="gateMode">Gate mode on or off.</param>
        /// <param name="continuousMode">Continuous mode on or off. </param>
        void SetTriggerState(EPulsGenArmingSource armSource, EState triggeredMode, EState gatedMode, EState continuosMode);

            /// <summary>
            ///     Resets the PulsFuncArbGen.
            /// </summary>
            void Reset();

        /// <summary>
        ///     Checks from instrument if previous operation is completed or not.
        /// </summary>
        /// <returns>The operation complete status</returns>
        EopcStatus Opc();

        /// <summary>
        ///     Set frequency for PulsFuncArbGen.
        /// </summary>
        /// <param name="freqInHz">Frequency in MHz</param>
        void SetFrequency(double freqInHz);

        /// <summary>
        ///     Get frequency from PulsFuncArbGen in MHz.
        /// </summary>
        double GetFrequency();

        /// <summary>
        ///     Set delay in seconds for PulsFuncArbGen.
        /// </summary>
        /// <param name="delInSec">Set delay in seconds.</param>
        void SetDelay(double delInSec);

        /// <summary>
        ///     Sets the amplitude to instrument.
        /// </summary>
        /// <param name="ampInVolts">Set amplitude in volts</param>
        void SetAmplitude(double ampInVolts);

        /// <summary>
        ///     Measures amplitude. Unit is volt
        /// </summary>
        /// <returns>Measured amplitude</returns>
        double GetAmplitude();

        /// <summary>
        ///     Sets Dc offset state to the instrument.
        /// </summary>
        /// <param name="offVolt">DC offset in volts</param>
        void SetDcOffset(double offVolt);

        /// <summary>
        ///     Sets the load impedance to the instrument.
        /// </summary>
        /// <param name="loadImp">Load impedance to be set in Ohm</param>
        void SetLoadImpedance(double loadImp);

        /// <summary>
        ///     Sets the output impedance to the instrument, options are 5 or 50 Ohm.
        /// </summary>
        /// <param name="outputImp">Output impedance to be set.</param>
        void SetOutputImpedance(double outputImp);

        /// <summary>
        /// Sets the pulse duty cycle percentage to the instrument.
        /// </summary>
        /// <param name="percentage">Duty cycle percentage to be set</param>
        void SetDutyCycle(decimal percentage);

        /// <summary>
        ///     Sets output polarity to normal or inverted.
        /// </summary>
        /// <param name="polarity">The state to be set</param>
        void SetPolarity(EPulsGenPolarity polarity);

        /// <summary>
        ///     Sets the output state to the instrument.
        /// </summary>
        /// <param name="outputState">The state to be set</param>
        void SetOutputState(EState outputState);

        /// <summary>
        ///     Sets the complement output state to the instrument.
        /// </summary>
        /// <param name="compState">The output complement state to be set</param>
        void SetComplement(EState compState);
    }
}

