﻿using System;
using Keysight.Tap;

namespace OpenTap.Plugins.Clock
{
    /// <summary>
    ///     This enum is used to inform, if all previously executed SCPI commands have been completed or not (=Incomplete).
    /// </summary>

    public enum EOutputPort
    {
        Out100MHzPort1,
        Out100MHzPort2,
        Out100MHzPort3,
        Out100MHzPort4,
        Out100MHzPort5,
        Out100MhzBpPort,
        Out10MhzPort,
        OutOcxoPort,
    }


    public interface IClock : IInstrument
    {
        /// <summary>
        ///     Queries instrument info
        /// </summary>
        /// <returns>Instrument information</returns>
        string Identify();

        /// <summary>
        ///    The entire error queue is read, then emptied from instrument
        /// </summary>
        /// <param name="error">Returns the error string</param>
        /// <returns>Gives the error occurred status</returns>
        bool SystemError(out string error);

        /// <summary>
        ///     Reset the reference clock board to default statue
        /// </summary>
        void Reset();

        /// <summary>
        ///     Self test instrument.
        /// </summary
        /// <param name="testMessage">Get the model self test message</param>
        /// <returns>Returns self test result status code(0:pass, other:fault code)</returns>
        int Selftest(out string testMessage);

        /// <summary>
        ///     Set reference clock output port status
        /// </summary>
        /// <param name="outputPort">output reference clock port</param>
        /// <param name="enable">enable statue[true:enable;false:disable] </param>
        void SetReferenceOutputStatus(EOutputPort outputPort, bool enable);

        /// <summary>
        ///     Get reference clock output port status
        /// </summary>
        /// <param name="outputPort">output reference clock port</param>
        /// <returns>return output port statue[true:enable;false:disable]</returns>
        bool GetReferenceOutputStatus(EOutputPort outputPort);

        /// <summary>
        ///     Set External reference clock
        /// </summary>
        /// <param name="refFreqMhz">Set External reference Frequency Rang[1, 110]MHz</param>
        /// <param name="useExtRef">true:use External reference and Freq , false:use internal reference  </param>
        void SetExternalReference(double refFreqMhz, bool useExtRef);

    }
}