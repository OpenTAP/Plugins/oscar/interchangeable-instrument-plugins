﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace OpenTap.Plugins
{
    class Trace
    {
        /// <summary>
        ///     Return caller method name. Usage: call GetCallername()
        /// </summary>
        /// <returns>Name of the caller method</returns>
        public static string GetCallerName([CallerMemberName] string memberName = "")
        {
            return memberName;
        }

        /// <summary>
        ///     Return callers full file name and path. Usage: call GetCallerFilePath()
        /// </summary>
        /// <returns>File name and path of a caller</returns>
        public static string GetCallerFilePath([CallerFilePath] string sourceFilePath = "")
        {
            return sourceFilePath;
        }

        /// <summary>
        ///     Return line number from which the method was called. Usage: call GetCallerLine()
        /// </summary>
        /// <returns>Callers line number</returns>
        public static int GetCallerLine([CallerLineNumber] int sourceLineNumber = 0)
        {
            return sourceLineNumber;
        }
    }
}