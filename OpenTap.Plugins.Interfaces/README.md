# Nokia.Tap.Driver.Interfaces

Assembly just for instrument and possibly dut control
It holds interfaces but also some classes that are placeholders. No functionality in this assembly.

## 21st of October Changes done in refactoring

- Common namespace created for enumerations that are common but needed in multiple interfaces. Namespace is Nokia.Tap.Driver.Interfaces.Cloud
- IMultimodeInstrument interface created to serve all instruments where some models have multipurpose and they need a function to change their mode. Namespace is Nokia.Tap.Driver.Interfaces.MultiModeInstrument
- ICloud was originally created for TIC's need. It holds only one method void Initialize(JObject configElement); It was intented to be used like Open() in tap-drivers. Currently unclear is this needed or not.
- IScpi interface created to harmonize naming of common scpi method names. Previously interfaces had various naming schemas for Reset, Clear, Idn, error querying. Namespace is Nokia.Tap.Driver.Interfaces.SCPI

List of changes in each interface:

#### IAcPsu

- EState removed, now in namespace Nokia.Tap.Driver.Interfaces.Common
- EOpcStatus removed, now in namespace Nokia.Tap.Driver.Interfaces.Common
- IdnString() removed, now in IScpi
- List<ScpiInstrument.ScpiError> QueryErrors(bool suppressLogMessages = false, int maxErrors = 1000) removed, now in IScpi
- Reset() removed, now in IScpi

#### IAcSafety

- IdnString() removed, now in IScpi
- List<ScpiInstrument.ScpiError> QueryErrors(bool suppressLogMessages = false, int maxErrors = 1000) removed, now in IScpi

#### IClock

- Identify() removed, now in IScpi as IdnString
- SystemError(out string error) removed, now in IScpi as List<ScpiInstrument.ScpiError> QueryErrors(bool suppressLogMessages = false, int maxErrors = 1000);
- Reset() removed, now in IScpi

#### ICounter

- EState removed, now in namespace Nokia.Tap.Driver.Interfaces.Common
- Identify() removed, now in IScpi as IdnString
- List<ScpiInstrument.ScpiError> QueryErrors(bool suppressLogMessages = false, int maxErrors = 1000) removed, now in IScpi
- Reset() removed, now in IScpi
- void SetInputChannelCoupling(ECouplingState couplingState, uint channel = 1); -> void SetInputChannelCoupling(ECouplingState couplingState);
- void SetImpedance(uint impedance, uint channel = 1); -> void SetImpedance(uint impedance);
- void SetInputLevelRelative(double inputThresholdPercentage, uint channel = 1); -> void SetInputLevelRelative(double inputThresholdPercentage)
- void SetInputLevelAutoState(EState state, uint channel = 1) -> void SetInputLevelAutoState(EState state)

#### IDatalogger

- enum Mode -> EMode
- enum Type -> EType
- enum ChannelCurrent -> EChannelCurrent
- enum MemoryLocation -> EMemoryLocation
- enum Channel removed, all methods are using channel as integer.
- EOpcStatus removed, now in namespace Nokia.Tap.Driver.Interfaces.Common
- SaveState(MemoryLocation location) -> SaveState(EMemoryLocation location)
- RecallState(MemoryLocation location) -> RecallState(EMemoryLocation location)
- MeasureCurrent(Mode acDc, double range, ChannelCurrent channel) -> MeasureCurrent(EMode acDc, double range, EChannelCurrent channel)
- MeasureVoltage(Mode acDc, double range, int channel) -> MeasureVoltage(EMode acDc, double range, int channel)
- DIO_Output(Type tType, int iData, int channel) -> DIO_Output(EType tType, int iData, int channel)
- DIO_Input(Type tType, int channel) -> DIO_Input(EType tType, int channel)
- DIO_OutputRead(Type tType, int channel) -> DIO_OutputRead(EType tType, int channel)
- List<ScpiInstrument.ScpiError> QueryErrors(bool suppressLogMessages = false, int maxErrors = 1000) removed, now in IScpi
- Reset() removed, now in IScpi
- IdnString() removed, now in IScpi
- EopcStatus Opc() removed, now in IScpi as WaitForOperationComplete

#### IDmm

- enum Mode -> EMode
- enum Channel -> EChannel
- Identify() removed, now in IScpi as IdnString
- EopcStatus Opc() removed, now in IScpi as WaitForOperationComplete
- Reset() removed, now in IScpi
- int Selftest(out string testMessage); -> int SelfTest(out string testMessage);
- double MeasureVoltage(Mode acDc, double range); -> double MeasureVoltage(EMode acDc, double range);
- double MeasureVoltage(Mode acDc, double range, Channel channel); -> double MeasureVoltage(EMode acDc, - double range, EChannel channel);
- double MeasureCurrent(Mode acDc, double range); -> double MeasureCurrent(EMode acDc, double range);
- double MeasureCurrent(Mode acDc, double range, ChannelCurrent channel); -> double MeasureCurrent(EMode acDc, double range, EChannelCurrent channel);
- double MeasureResistance(Channel channel); -> double MeasureResistance(EChannel channel);
- double MeasureResistance(Channel channel); -> double MeasureResistance(EChannel channel);

#### IDutControl

- EState removed, now in namespace Nokia.Tap.Driver.Interfaces.Common
- int GetPAOCPStatus(uint pa); -> int GetPaOcpStatus(uint pa);
- int GetAldpGoodStatus(string device); -> int GetAldPgoodStatus(string device);

#### IEload

- EState removed, now in namespace Nokia.Tap.Driver.Interfaces.Common
- EErrorOccurred removed, not needed
- EOpcStatus removed, now in namespace Nokia.Tap.Driver.Interfaces.Common
- void Clear(); removed, now in IScpi
- string Identify(); removed, now in IScpi as IdnString
- EopcStatus Opc(); removed, now in IScpi as WaitForOperationComplete
- void Wait(); removed, is this really needed? Can be replaced with WaitForOperationComplete??
- bool SystemError(out string response); removed, now in IScpi as List<ScpiInstrument.ScpiError> QueryErrors(bool suppressLogMessages = false, int maxErrors = 1000)
- void EloadReset(); removed, now in IScpi as Reset()
- int Selftest(out string message); -> int SelfTest(out string message);

#### IFactoryRa

- enum RequestType -> enum ERequestType
- enum CmdErrorCode -> enum ECmdErrorCode

#### II2C

- enum ESpiMode removed, is not for I2C
- enum ESpiBitOrder removed, is not for I2C
- enum ESpiChipSelect removed, is not for I2C
- enum ESpiChipSelectPolarity removed, is not for I2C
- enum EState removed, now in namespace Nokia.Tap.Driver.Interfaces.Common
- void SlaveEnable(byte slaveAddress, ushort maxTxBytes, ushort maxRxBytes); added
- void SlaveDisable(); added
- byte[] SlaveRead(byte slaveAddress, ushort numOfBytesMax, out int numOfBytesRead); added

#### INa, most likely one of the trickiest ones!

- enum EErrorOccurred removed
- EState removed, now in namespace Nokia.Tap.Driver.Interfaces.Common
- enum DisplayState -> enum EDisplayState
- enum SegmentDisplayState -> enum ESegmentDisplayState
- enum ChannelLayouts -> enum EChannelLayouts
- enum LimitLineType -> enum ELimitLineType
- enum BufSetting -> enum EBufSetting
- enum StimulusSetting -> enum EStimulusSetting
- enum CharacterizationType -> enum ECharacterizationType
- enum TriggerSource -> enum ETriggerSource
- enum EopcStatus removed, now in namespace Nokia.Tap.Driver.Interfaces.Common
- enum ECalibrationKitConnectorType, added
- bool SystemError(out string response); removed, now in IScpi as List<ScpiInstrument.ScpiError> QueryErrors(bool suppressLogMessages = false, int maxErrors = 1000)
- void SetMarkerSearch(uint channel, string command); removed
- Complex[,] ReadAllSparamData(uint channel, List<string> sparams, out double[] frequencies); -> Complex[,] ReadAllSparamData(uint channel, List<ESparameter> sparams, out double[] frequencies);
- string Identify(); removed, now in IScpi as IdnString
- void RestartSweep(uint channel); added
- void PresetEna(); removed, now in IScpi as Reset()
- bool Perform3PortCalibration(uint channelNo, out string response); removed
- bool Perform4PortCalibration(uint channelNo, out string response); removed
- void PerformCalibration(uint channelNo, ECalibrationStandard standard = ECalibrationStandard.SHOR, ECalibrationType  portTypeName = ECalibrationType.Tosm1Port); removed
- void SetTriggerSource(TriggerSource triggerSource); -> void SetTriggerSource(ETriggerSource triggerSource);
- int OperationCompleted(); removed, now in IScpi as WaitForOperationComplete
- void SetupCalibration(NaCalibrationParams parameters); added
- void SetErrorDisplay(EState state); added
- void SetPowerLevel(uint channel, decimal powerDbm); added
- void ConfigureCalibrationKit(ECalibrationKitConnectorType connectorType, string characterizationFileName = ""); added
- bool LimitLineCheck(uint channel, uint traceNo); added
- bool IsCalibrationValid(string testerName, string productCode, uint calibrationLifeCycle, bool useProductCodeVersionInfo = false); added
- void UpdateStateCalibrationInfo(string productCode, string stateFileName, DateTime dateTime); added
- void SetDisplayUpdate(EState state); added
- void UpdateDisplay(); added
- void DisableAllMarkers(uint channel); added
- void AutoScale(uint channel, uint trace); added
- void GetStartAndStopFrequency(uint channel, out decimal startFreqMhz, out decimal stopFreqMhz, uint segment = 0); added
- void SetStimStartStop(uint channelNo, string freqStart, string freqStop); removed, duplicate to void SetStartAndStopFrequency(uint channel, decimal startMhz, decimal stopMhz);
- double ReadMarkerResult(uint channelNo, uint MarkerNo); removed duplicate to decimal Measure(uint channel, ESparameter sparam, decimal startFrequencyMHz, decimal stopFrequencyMHz, EMeasurementType measurementType, out decimal frequencyMHz);
- void SetTriggerSingleWait(uint channelNo); removed, dupliate to void Sweep(uint channel);

bool GetCalibData(uint naCalibrationSpan, string setupFileName, string mappedDriveForNaCalibData); This is missing, ask Gavin about it!!

#### IPib

- EState removed, now in namespace Nokia.Tap.Driver.Interfaces.Common
- enum FixPsuVoltages -> enum EFixPsuVoltages
- enum PolarityInfo -> enum EPolarityInfo
- enum PolaritySet -> enum EPolaritySet
- string IdQuery(); removed, now in IScpi as IdnString

#### IPm

- EOpcStatus removed, now in namespace Nokia.Tap.Driver.Interfaces.Common
- EState removed, now in namespace Nokia.Tap.Driver.Interfaces.Common
- string IdQuery(); removed, now in IScpi as IdnString
- List<ScpiInstrument.ScpiError> QueryErrors(bool suppressLogMessages = false, int maxErrors = 1000) removed, now in IScpi
- void SetAverageManual(int averageCount = 9); -> void SetAverageManual(int averageCount);
- void ActivateNsRatioAutoAveraging(double maxMeasTimeS, double nsRatio); added
- void MoveToPosition(double x, double y, bool relative, bool wait = true); added
- void MoveToXPosition(double x, bool relative, bool wait = true); added
- void MoveToYPosition(double y, bool relative, bool wait = true); added

#### IPulsGen

- Name of the interface is changed to IPulseGen
- EState removed, now in namespace Nokia.Tap.Driver.Interfaces.Common
- enum EErrorOccurred removed
- EOpcStatus removed, now in namespace Nokia.Tap.Driver.Interfaces.Common
- List<ScpiInstrument.ScpiError> QueryErrors(bool suppressLogMessages = false, int maxErrors = 1000) removed, now in IScpi
- void Reset(); removed, now in IScpi
- EopcStatus Opc() removed, now in IScpi as WaitForOperationComplete

#### ISa

- EState removed, now in namespace Nokia.Tap.Driver.Interfaces.Common
- public enum EInstrumentMode removed, now in namespace Nokia.Tap.Driver.Interfaces.MultiModeInstrument
- EopcStatus removed, now in namespace Nokia.Tap.Driver.Interfaces.Common
- enum RoscSource -> enum ERoscSource
- enum AcpResultType -> enum EAcpResultType
- enum AcpResultMode -> enum EAcpResultMode
- void Clear(); removed, now in IScpi
- List<ScpiInstrument.ScpiError> QueryErrors(bool suppressLogMessages = false, int maxErrors = 1000) removed, now in IScpi
- EopcStatus Opc() removed, now in IScpi as WaitForOperationComplete
- SetInstrumentMode(EInstrumentMode instrumentMode); removed, now in IMultiModeInstrument
- RestartSweep() -> TriggerSweep()
- void SendScpiCommands(string[] scpiCommands); removed
- SetCcorEnable(bool enable); -> SetComplexCorrectionState(EState state);
- SetCorrEnable(bool enable, int corrSetNum); -> SetCorrectionState(EState state, int corrSetNum);
- void GetMarkerMax(int markerNum, out double freqMhz, out double powerDbm); added
- void ConfigCcdfMeasure(decimal rbwHz, uint measurementPoints, decimal probability); added
- void SetSweepPoints(uint points); added
- void SetAverage(uint averageCount); added
- void SetAverageState(EState state); added

#### IScope

- EState removed, now in namespace Nokia.Tap.Driver.Interfaces.Common
- List<ScpiInstrument.ScpiError> QueryErrors(bool suppressLogMessages = false, int maxErrors = 1000) removed, now in IScpi

#### ISg

- EState removed, now in namespace Nokia.Tap.Driver.Interfaces.Common
- EopcStatus removed, now in namespace Nokia.Tap.Driver.Interfaces.Common
enum EArbRefSource added
- List<ScpiInstrument.ScpiError> QueryErrors(bool suppressLogMessages = false, int maxErrors = 1000) removed, now in IScpi
- void Reset(); removed, now in IScpi
- EopcStatus Opc() removed, now in IScpi as WaitForOperationComplete

#### ISpi, Spi methods taken from old ISpi and II2cSpi

- enum ESpiSsPolarity added
- enum ETargetPower added
- enum ESpiSleepMode added
- enum ESpiSsSignal added
- void SpiInit(ESpiMode mode, ESpiBitOrder bitOrder, ESpiChipSelect chipSelect, ESpiChipSelectPolarity csPolarity, double bitRate); -> Init(.......)
- void Init(ESpiMode mode, ESpiBitOrder bitOrder, ESpiSsPolarity eSsPolarity); added
byte[] SpiWrite(byte[] data); -> byte[] Query(byte[] data);
- bool CheckBitRateValueValid(uint bitRatekHz); added
- void SetTargetPower(ETargetPower targetPower); added
- void Delay(int value, ESpiSleepMode sleepMode); added
- void SetBitRate(uint bitrateKhz); added
- byte[] Query(byte[] data, ESpiSsSignal eSsSignal); added
- void SlaveEnable();
- void SlaveDisable();
- byte[] SlaveRead(ushort numOfBytesMax, out int numOfBytesRead);
- int SlaveSetResponse(byte[] dataToResponse);

#### IArbGen made obsolete.

Todo:

- IDmm : bool SystemError(out string error); -> QueryErrors
- IDutControl : ConnectDutSorceAddress typo
- INa : enum TraceFormat ja enum ETraceFormat samassa interfacessa
- INa : enum SweepMode ja enum ESweepMode samassa interfacessa


## Getting Started

Tap developer guide: https://www.keysight.com/upload/cmc_upload/All/TapDeveloperGuide.pdf
Markdown cheat sheet, that is useful when editing this file: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet


### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

### Installing

A step by step series of examples that tell you have to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Projects LINKS HERE!

## Namespaces

## Deployment

Add additional notes about how to deploy this on a live system

## Built With


## Contributing

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlabe1.ext.net.nokia.com/oscar/Nokia.Tap.StdLib/tags). 

## Authors

* **Oscar Team** - *Initial work* - (https://gitlabe1.ext.net.nokia.com/groups/oscar/group_members)

See also the list of [contributors](https://gitlabe1.ext.net.nokia.com/oscar/Tap.Drivers/Nokia.Tap.Driver.Interfaces/network/master) who participated in this project.

## License


## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc
