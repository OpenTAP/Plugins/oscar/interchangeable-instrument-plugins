﻿using OpenTap.Plugins.Interfaces.Common;
using OpenTap.Plugins.Interfaces.SCPI;

namespace OpenTap.Plugins.Interfaces.PM
{
    public interface IPm : IInstrument, IScpi
    {
        /// <summary>
        ///     This command causes PM to perform its auto-zeroing routine ONCE.
        /// </summary>
        /// <remarks>Keysight U2022XA can run autocalibration within Zero-cal, if wanted. Choosing is available in TAP: Settings/Bench/Instruments</remarks>
        void ExecuteZeroCompensation();

        /// <summary>
        ///     Sets the measuring frequency to be measured and executes measurement.
        /// </summary>
        /// <param name="frequencyKHz">Measuring frequency in kHz.</param>
        /// <returns>Measurement result</returns>
        /// <remarks>Measurement needs configurations to be done before run the measure.</remarks>
        double MeasureBurstPower(long frequencyKHz);

        /// <summary>
        ///     Sets the measuring frequency to be measured and executes measurement.
        /// </summary>
        /// <param name="frequencyKHz">Measuring frequency in kHz.</param>
        /// <returns>Measurement result</returns>
        /// <remarks>Measurement needs configurations to be done before run the measure.</remarks>
        double MeasurePower(long frequencyKHz);

        /// <summary>
        /// Defines a fixed offset in dB, which is used to correct the measured value. (When a log
        /// scale is used, the offset is added to the measured value; this is the reason why the
        /// </summary>
        /// <param name="state">Defines if offset is in use or not.</param>
        /// <param name="offsetDb">The offset value in dB.</param>
        void SetOffSetValue(EState state, double offsetDb);

        /// <summary>
        ///     This command enables automatic averaging.
        /// </summary>
        void SetAverageAuto();

        /// <summary>
        ///     This command disables averaging and set detector Measurement mode to NORMal.
        /// </summary>
        void SetAverageOff();

        /// <summary>
        ///     Activates manual averaging with given average counter value.
        ///     If average count is 1, averaging is not used.
        /// </summary>
        /// <param name="averageCount">The amount of averages to be used. [1 to 1024]</param>
        void SetAverageManual(int averageCount);

        /// <summary>
        /// Activates auto averaging with given parameters.
        /// </summary>
        /// <param name="maxMeasTimeS">The maximum measurement time (Seconds).</param>
        /// <param name="nsRatio">NSRatio determines the relative noise component in the measurement
        /// result.The noise component is defined as the magnitude of the level variation in dB caused
        /// by the inherent noise of the sensor (two standard deviations).</param>
        void ActivateNsRatioAutoAveraging(double maxMeasTimeS, double nsRatio);
    }
}
