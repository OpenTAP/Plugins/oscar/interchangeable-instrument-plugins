﻿using System.Collections.Generic;

namespace OpenTap.Plugins.Interfaces.LedAnalyzer
{
    public enum EBaudRate
    {
        [Display("9600")]
        BaudRate9600 = 9600,
        [Display("14400")]
        BaudRate14400 = 14400,
        [Display("19200")]
        BaudRate19200 = 19200,
        [Display("57600")]
        BaudRate57600 = 57600,
        [Display("115200")]
        BaudRate115200 = 115200,
        [Display("460800")]
        BaudRate460800 = 460800,
    }
    public enum EDataBits
    {
        [Display("Bit7")]
        Bit7 = 7,
        [Display("Bit8")]
        Bit8 = 8
    }

    /// <summary>
    /// Target brightness Level used when scan
    /// </summary>
    public enum EBrightnessLevel
    {
        Auto = 0,
        Low = 1,
        Medium = 2,
        High = 3,
        Super = 4,
        Ultra = 5
    }

    /// <summary>
    /// RGB data class
    /// </summary>
    public class RgbData
    {
        public byte Red;
        public byte Green;
        public byte Blue;
    }

    /// <summary>
    /// HSI data class
    /// </summary>
    public class HsiData
    {
        public double Hue;
        public int Saturation;
        public int Intensity;
    }

    /// <summary>
    /// Chromaticity data class
    /// </summary>
    public class ChromaticityData
    {
        public double XChromaticity;
        public double YChromaticity;
    }

    public interface ILedAnalyzer : IInstrument
    {
        /// <summary>
        /// Initialize the Led Analyzer device
        /// </summary>
        void Initialize();

        /// <summary>
        /// Trigger one scan for all channels
        /// </summary>
        /// <param name="brightnessLevel">Target brightness level</param>
        void CaptureData(EBrightnessLevel brightnessLevel);

        /// <summary>
        /// Trigger one scan for PWM Led
        /// </summary>
        /// <param name="brightnessLevel">Target brightness level</param>
        /// <param name="averageFactor"> default is 0 which means no average</param>
        void CaptureDataPwm(EBrightnessLevel brightnessLevel, int averageFactor = 0);

        /// <summary>
        /// Get RGB value after scan
        /// </summary>
        /// <param name="channel">channel(fiber) ID in the device</param>
        /// <returns></returns>
        RgbData GetRgb(int channel);

        /// <summary>
        /// Get RGB values
        /// </summary>
        List<RgbData> GetRgbs();

        /// <summary>
        /// Get HSI value after scan
        /// </summary>
        /// <param name="channel">channel(fiber) ID in the device</param>
        /// <returns></returns>
        HsiData GetHsi(int channel);

        /// <summary>
        /// Get GetHsiGroup value after scan
        /// </summary>
        /// <param name="channelgroup">group ID in the device</param>
        /// <returns></returns>
        List<HsiData> GetHsiGroup(int channelgroup);

        /// <summary>
        /// Get chromaticity value for group of channel
        /// </summary>
        /// <param name="channel">group of channel(fiber) in the device, 0 or 1</param>
        /// <returns></returns>

        ChromaticityData GetChromaticity(int channel);
    }
}