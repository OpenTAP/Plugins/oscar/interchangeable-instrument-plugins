﻿using System.Collections.Generic;

namespace OpenTap.Plugins.Interfaces.DIO
{
    #region enums
    public enum EThreshold
    {
        Threshold1,
        Threshold2
    }

    public enum EInputState
    {
        Low,
        High,
        Middle
    }

    public enum EOutputState
    {
        Sink,
        Source,
        Off
    }
    #endregion

    public interface IDio : IInstrument
    {
        /// <summary>
        ///     Queries instrument info
        /// </summary>
        /// <returns>Instrument information</returns>
        string Identify();

        /// <summary>
        ///     The entire error queue is read, then emptied from instrument
        /// </summary>
        /// <param name="error">Returns the error string</param>
        /// <returns>Gives the error occurred status</returns>
        bool SystemError(out string error);

        /// <summary>
        ///     Reset the reference dio board to default statue
        /// </summary>
        void Reset();

        /// <summary>
        ///     Self test instrument.
        /// </summary>
        /// <param name="testMessage">Get the module self test message</param>
        /// <returns>Returns self test result status code(0:pass, other:fault code)</returns>
        int SelfTest(out string testMessage);

        /// <summary>
        ///     Set the desired threshold levels,keysight M9187A threshold is dual
        /// </summary>
        /// <param name="levels">list of level, keysight M9187A threshold amount is 2</param>
        void SetThresholdLevel(List<double> levels);

        /// <summary>
        ///     get the  threshold level as a list of double types
        /// </summary>
        /// <param name="thresholds">list of threshold number</param>
        /// <returns>List of threshold levels</returns>
        List<double> GetThresholdLevel(List<EThreshold> thresholds);

        /// <summary>
        ///     get input state as an array of enumerated types,keysight M9187A module can be configured as 32 channels of input
        /// </summary>
        /// <param name="channels">list of channel id,Numeric support ranges from 1 to 32</param>
        /// <returns> List of input states</returns>
        List<EInputState> GetInputState(List<short> channels);

        /// <summary>
        ///     set output state,keysight M9187A module can be configured as 32 channels of input and output
        /// </summary>
        /// <param name="channels">list of channel id,Numeric support ranges from 1 to 32 </param>
        /// <param name="states">set output state as a list of enumerated type,the state corresponds to channel one to one</param>
        void SetOutputState(List<short> channels, List<EOutputState> states);

        /// <summary>
        ///     get the output state as a list of enumerated types,keysight M9187A module can be configured as 32 channels of
        ///     output
        /// </summary>
        /// <param name="channels">list of channel id,Numeric support ranges from 1 to 32 </param>
        /// <returns>List of output states</returns>
        List<EOutputState> GetOutputState(List<short> channels);
    }
}
