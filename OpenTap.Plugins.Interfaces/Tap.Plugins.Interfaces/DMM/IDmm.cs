﻿using OpenTap.Plugins.Interfaces.SCPI;

namespace OpenTap.Plugins.Interfaces.DMM
{
    #region enums
    /// <summary>
    ///     This enum is used to inform, if all previously executed SCPI commands have been completed or not (=Incomplete).
    /// </summary>
    public enum EMode
    {
        Ac,
        Dc
    }

    public enum EChannel
    {
        Channel1 = 101,
        Channel2 = 102,
        Channel3 = 103,
        Channel4 = 104,
        Channel5 = 105,
        Channel6 = 106,
        Channel7 = 107,
        Channel8 = 108,
        Channel9 = 109,
        Channel10 = 110,
        Channel11 = 111,
        Channel12 = 112,
        Channel13 = 113,
        Channel14 = 114,
        Channel15 = 115,
        Channel16 = 116,
        Channel17 = 117,
        Channel18 = 118,
        Channel19 = 119,
        Channel20 = 120,
        Channel201 = 201,
        Channel202 = 202,
        Channel203 = 203,
        Channel204 = 204,
        Channel205 = 205,
        Channel206 = 206,
        Channel207 = 207,
        Channel208 = 208,
        Channel209 = 209,
        Channel210 = 210,
        Channel211 = 211,
        Channel212 = 212,
        Channel213 = 213,
        Channel214 = 214,
        Channel215 = 215,
        Channel216 = 216,
        Channel217 = 217,
        Channel218 = 218,
        Channel219 = 219,
        Channel220 = 220
    }

    public enum EChannelCurrent
    {
        Channel20 = 121,
        Channel21 = 122
    }
    #endregion

    public interface IDmm : IInstrument, IScpi
    {
        /// <summary>
        /// Prohibits the instrument from executing any new commands until all pending
        /// overlapped commands have been completed.
        /// </summary>
        void Wait();

        /// <summary>
        ///    The entire error queue is read, then emptied from instrument
        /// </summary>
        /// <param name="error">Returns the error string</param>
        /// <returns>Gives the error occurred status</returns>

        /// <summary>
        /// Self test instrument.
        /// </summary>
        /// <param name="testMessage">Get the model self test message</param>
        /// <returns>Returns self test result status code(0:pass, other:fault code)</returns>
        int SelfTest(out string testMessage);

        /// <summary>
        /// Measure voltage.
        /// </summary>
        /// <param name="acDc">Set voltage measurement to DC or AC mode</param>
        /// <param name="range">Set voltage measurement range, give maximum voltage value you expect to measure. Range is selected to match according to Dmm model ranges.</param>
        /// <returns>Returns an measured voltage in Volts</returns>
        double MeasureVoltage(EMode acDc, double range);

        /// <summary>
        /// Measure voltage.
        /// </summary>
        /// <param name="acDc">Set voltage measurement to DC or AC mode</param>
        /// <param name="range">Set voltage measurement range, give maximum voltage value you expect to measure. Range is selected to match according to Dmm model ranges.</param>
        /// <param name="channel">Set measurement channel. Use parameter Channel1 for all other DMMs than Keysight 34972A.</param>
        /// <returns>Returns an measured voltage in Volts</returns>
        double MeasureVoltage(EMode acDc, double range, EChannel channel);

        /// <summary>
        /// Measure current.
        /// </summary>
        /// <param name="acDc">Set current measurement to DC or AC mode.</param>
        /// <param name="range">Set current measurement range, give maximum current value you expect to measure. Range is selected to match according to Dmm model ranges.</param>
        /// <returns>Returns an measured current in Amps</returns>
        double MeasureCurrent(EMode acDc, double range);

        /// <summary>
        /// Measure current.
        /// </summary>
        /// <param name="acDc">Set current measurement to DC or AC mode.</param>
        /// <param name="range">Set current measurement range, give maximum current value you expect to measure. Range is selected to match according to Dmm model ranges.</param>
        /// <param name="channel">Set measurement channel for the current mode. Use parameter Channel20 for all other DMMs than Keysight 34972A.</param>
        /// <returns>Returns an measured current in Amps</returns>
        double MeasureCurrent(EMode acDc, double range, EChannelCurrent channel);

        /// <summary>
        /// Measure resistance in 2-Wire mode.
        /// </summary>
        /// <returns>Returns an measured resistance in Ohms</returns>
        double MeasureResistance();

        /// <summary>
        /// Measure resistance in 2-Wire mode.
        /// </summary>
        /// <param name="range">Set voltage measurement range, give maximum voltage value you expect to measure. Range is selected to match according to Dmm model ranges.</param>
        /// <returns>Returns an measured resistance in Ohms</returns>
        double MeasureResistance(double range);

        /// <summary>
        /// Measure resistance in 2-Wire mode.
        /// </summary>
        /// <param name="channel">Set measurement channel for the resistance 2-Wire mode. Use parameter Channel1 for all other DMMs than Keysight 34972A.</param>
        /// <returns>Returns an measured resistance in Ohms</returns>
        double MeasureResistance(EChannel channel);

        /// <summary>
        /// Measure Capacitance
        /// </summary>
        /// <param name="range">Set Capacitance measurement range, give maximum capacitance value you expect to measure. Range is selected to match according to Dmm model ranges.</param>
        /// <returns>Returns an measured capacitance in mF</returns>
        double MeasureCapacitance(double range);

        /// <summary>
        /// Measure Temperature
        /// </summary>
        /// <returns>Returns an measured Temperature in oC</returns>
        double MeasureTemperature();

        /// <summary>
        /// Measure spell
        /// </summary>
        /// <param name="range">Set Freq measurement range, give maximum Frequency value you expect to measure. Range is selected to match according to Dmm model ranges.</param>
        /// <param name="voltage">Set Frequency measurement voltage range(unit:V), give maximum Voltage value you expect to measure. Range is selected to match according to Dmm model ranges.</param>
        /// <returns>Returns an measured Frequency in Hz</returns>
        double MeasureFrequency(double range, double voltage);

        /// <summary>
        /// Measure Period
        /// </summary>
        /// <param name="range">Set Period measurement range, give maximum Period value you expect to measure. Range is selected to match according to Dmm model ranges.</param>
        /// <param name="voltage">Set Period measurement voltage range(unit:mV), give maximum Voltage value you expect to measure. Range is selected to match according to Dmm model ranges.</param>
        /// <returns>Returns an measured Period in msec</returns>
        double MeasurePeriod(double range, double voltage);
    }
}
