﻿namespace OpenTap.Plugins.Interfaces.PIM
{
    #region enums
    public enum EOperationMode
    {
        Remote,
        Local
    }

    public enum EFreqIndex
    {
        Freq1,
        Freq2
    }

    public enum EReceiverUnit
    {
        dBc,
        dBm
    }

    public enum EDetectorMode
    {
        Peak,
        Average
    }

    public enum EBaudRate
    {
        B9600,
        B19200,
        B38400,
        B57600
    }

    public enum EBand
    {
        DCS = 0,
        PCS,
        UMTS,
        TD_SCDMA_A,
        TD_SCDMA_F,
        FR1822
    }
    #endregion

    public interface IPim : IInstrument
    {
        /// <summary>
        /// Sets operation mode for Pim analyzer.
        /// Local=Enabled frontpanel keys
        /// Remote=DIsables formtpanel keys
        /// </summary>
        /// <param name="mode">Remote or Local</param>
        void SetOperationMode(EOperationMode mode);

        /// <summary>
        /// Sets frequency for f1 or f2.
        /// </summary>
        /// <param name="freqIndex">Freq1 or Freq2</param>
        /// <param name="freq">Frequency in Mhz</param>
        void SetFrequency(EFreqIndex freqIndex, double freq);

        /// <summary>
        /// Sets start frequency for f1 or f2.
        /// </summary>
        /// <param name="freqIndex">Freq1 or Freq2</param>
        /// <param name="startFreq">Frequency in Mhz</param>
        void SetStartFrequency(EFreqIndex freqIndex, double startFreq);

        /// <summary>
        /// Sets stop frequency for f1 or f2.
        /// </summary>
        /// <param name="freqIndex">Freq1 or Freq2</param>
        /// <param name="stopFreq">Frequency in Mhz</param>
        void SetStopFrequency(EFreqIndex freqIndex, double stopFreq);

        /// <summary>
        /// Sets step size in Mhz
        /// </summary>
        /// <param name="stepSize">0.1 - 9.9 Mhz</param>
        void StepSize(double stepSize);

        /// <summary>
        /// Switch to available frequency band.
        /// </summary>
        /// <param name="band"></param>
        void SetFrequencyBand(EBand band);

        /// <summary>
        /// Sets power in 0.1 dBm
        /// </summary>
        /// <param name="freqIndex">Freq1 or Freq2</param>
        /// <param name="power">Power in dBm, 0.1dBm</param>
        void SetPower(EFreqIndex freqIndex, double power);

        /// <summary>
        /// Sets amplifier on/off
        /// </summary>
        /// <param name="freqIndex">f1 or f2</param>
        /// <param name="onOff">true or false</param>
        void SetAmplifierOnOff(EFreqIndex freqIndex, bool onOff);

        /// <summary>
        /// Sets PIM measurement mode
        /// </summary>
        void SetModePimMeasurement();

        /// <summary>
        /// Sets reflected PIM measurement mode
        /// </summary>
        void SetReflectedPimMeasurement();

        /// <summary>
        /// Sets Transmitted PIM measurement mode
        /// </summary>
        void SetTransmittedPimMeasurement();

        /// <summary>
        /// Sets 2-Ton measurement on/off
        /// </summary>
        /// <param name="onOff">True or false</param>
        void Set2TonMeasurementOnOff(bool onOff);

        /// <summary>
        /// Sets duration of 2-Ton measurement
        /// </summary>
        /// <param name="seconds">Time in seconds 1-30s</param>
        void Set2TonMeasurementDuration(ushort seconds);

        /// <summary>
        /// Sets IM-order: 3, 5, 7, 9, 11 ...
        /// </summary>
        /// <param name="imOrder"></param>
        void UseInternalReceiver(ushort imOrder);

        /// <summary>
        /// Starts sweep measurement.
        /// </summary>
        void StartSweepMeasurement();

        /// <summary>
        /// Sets ALC voltage manually for both synthesizers.
        /// </summary>
        /// <param name="mV">ALC voltage in mV ( 0-4095 ).</param>
        void SetManuallyVoltageForBothSynthesizers(ushort mV);

        /// <summary>
        /// Disables/enables band select button.
        /// </summary>
        /// <param name="disable">True or false.</param>
        void DisableBandSelectButton(bool disable);

        /// <summary>
        /// Disables/enables Sweep button.
        /// </summary>
        /// <param name="disable">True or false.</param>
        void DisableSweepButton(bool disable);

        /// <summary>
        /// Sets unit of internal receiver.
        /// </summary>
        /// <param name="unit">dBc or dBm.</param>
        void SetInternalReceiverUnit(EReceiverUnit unit);

        /// <summary>
        /// Sets baudrate: 0=9699 1=19200 2=38400 3=57600
        /// </summary>
        /// <param name="baudRate"></param>
        void SetBaudrate(EBaudRate baudRate);

        /// <summary>
        /// Sets detector mode.
        /// </summary>
        /// <param name="mode">Peak or Average</param>
        void SetDetectorMode(EDetectorMode mode);

        /// <summary>
        /// Gets device info.
        /// </summary>
        /// <returns></returns>
        string IdnString { get; }

        /// <summary>
        ///
        /// </summary>
        /// <param name="freqIndex"></param>
        /// <param name="frequency"></param>
        /// <param name="startFrequency"></param>
        /// <param name="stopFrequency"></param>
        void GetFrequencies(EFreqIndex freqIndex, out double frequency, out double startFrequency, out double stopFrequency);

        /// <summary>
        /// Gets stepsize in Mhz
        /// </summary>
        /// <returns></returns>
        double GetStepSize();

        /// <summary>
        /// Get status of Pim analyzer.
        /// </summary>
        /// <returns>status byte</returns>
        byte GetStatus();

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        EBand GetFrequencyBand();

        /// <summary>
        /// Get power level from f1 or f2.
        /// </summary>
        /// <param name="freqIndex">Freq1 or Freq2</param>
        /// <returns></returns>
        double GetPower(EFreqIndex freqIndex);

        /// <summary>
        /// Gets amplifier on/off status from f1 or f2.
        /// </summary>
        /// <param name="freqIndex">Freq1 or Freq2</param>
        /// <returns></returns>
        bool GetAmplifierOnOff(EFreqIndex freqIndex);

        /// <summary>
        /// Gets current IM-order and level.
        /// </summary>
        /// <param name="imOrder"></param>
        /// <param name="receiverLevel"></param>
        void GetImOrderAndReceiverLevel(out ushort imOrder, out double receiverLevel);

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        double GetReceiverAdcVoltage();
    }
}
