﻿using System.Collections.Generic;

namespace OpenTap.Plugins.Pm
{
    /// <summary>
    ///     This enum is used to inform, if all previously executed SCPI commands have been completed or not (=Incomplete).
    /// </summary>
    public enum EopcStatus
    {
        Incomplete = 0,
        Complete = 1
    }
    public enum EState
    {
        Off,
        On
    }

    public interface IPm : IInstrument
    {
        /// <summary>
        ///     This command causes PM to perform its auto-zeroing routine ONCE.
        /// </summary>
        /// <remarks>Keysight U2022XA can run autocalibration within Zero-cal, if wanted. Choosing is available in TAP: Settings/Bench/Instruments</remarks>
        void ExecuteZeroCompensation();

        /// <summary>
        ///     Queries instrument identify information
        /// </summary>
        /// <returns>Instrument ID-information</returns>
        string IdnString();

        /// <summary>
        ///     Sets the measuring frequency to be measured and executes measurement.
        /// </summary>
        /// <param name="frequencyKHz">Measuring frequency in kHz.</param>
        /// <returns>Measurement result</returns>
        /// <remarks>Measurement needs configurations to be done before run the measure.</remarks>
        double MeasureBurstPower(long frequencyKHz);

        /// <summary>
        ///     Sets the measuring frequency to be measured and executes measurement.
        /// </summary>
        /// <param name="frequencyKHz">Measuring frequency in kHz.</param>
        /// <returns>Measurement result</returns>
        /// <remarks>Measurement needs configurations to be done before run the measure.</remarks>
        double MeasurePower(long frequencyKHz);

        /// <summary>
        /// Defines a fixed offset in dB, which is used to correct the measured value. (When a log
        /// scale is used, the offset is added to the measured value; this is the reason why the
        /// </summary>
        /// <param name="state">Defines if offset is in use or not.</param>
        /// <param name="offsetDb">The offset value in dB.</param>
        void SetOffSetValue(EState state, double offsetDb);

        /// <summary>
        ///     Returns all errors in the instrument error stack. Clears the list with same call.
        /// </summary>
        /// <param name="suppressLogMessages">If true, the errors will not be logged</param>
        /// <param name="maxErrors">The max number of errors to retrieve. Useful if instrument generates errors faster than they can be read.</param>
        /// <returns>List of errors as follow: Code, Error message, a string formatted the same way as the output of SYST:ERR?</returns>
        List<ScpiInstrument.ScpiError> QueryErrors(bool suppressLogMessages = false, int maxErrors = 1000);

        /// <summary>
        ///     This command enables automatic averaging.
        /// </summary>
        void SetAverageAuto();

        /// <summary>
        ///     This command disables averaging and set detector Measurement mode to NORMal.
        /// </summary>
        void SetAverageOff();

        /// <summary>
        ///     Activates manual averaging with given average counter value. 
        ///     If average count is 1, averaging is not used.
        /// </summary>
        /// <param name="averageCount">The amount of averages to be used. [1 to 1024]</param>
        void SetAverageManual(int averageCount = 9);
    }
}